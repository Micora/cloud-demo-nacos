package cn.itcast.user.web;

import cn.itcast.user.config.PatternProperties;
import cn.itcast.user.pojo.User;
import cn.itcast.user.service.UserService;
import com.netflix.ribbon.proxy.annotation.Http;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
@RestController
@RequestMapping("/user")
//@RefreshScope  //添加热更新，也可以在config文件下写相关配置属性通过自动注入替代
public class UserController {

    @Autowired
    private UserService userService;

    //通过自动注入的方式添加配置属性，实现热更新
    @Autowired
    private PatternProperties properties;

//    读取写在nacos配置管理中的配置，该配置在nacos管理页面添加
//    @Value("${pattern.dateformat}")
//    private String dateformat;

    @GetMapping("prop")
    public PatternProperties properties() {
        return properties;
    }

    @GetMapping("now")
    public String now() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(properties.getDateformat()));
    }

    /**
     * 路径： /user/110
     *
     * @param id 用户id
     * @return 用户
     */
    @GetMapping("/{id}")
    public User queryById(@PathVariable("id") Long id,
                          @RequestHeader(value = "truth", required = false) String truth,
                          @RequestHeader(value = "RealTruth",required = false)String realTruth) {
        System.out.println("truth:"+truth);
        System.out.println("realTruth:"+realTruth);
        return userService.queryById(id);
    }
}
