# cloud-demo-nacos

#### 介绍
SpringCloud通过nacos集群（+nginx）实现的服务器注册和发现,额外包含了Feign远程调用、gateway网关

#### 软件架构
SpringCloud+nacos+feign


#### 使用说明
微服务（集群）（service） 处理请求，与数据库交互  
Nacos（集群+nginx） 注册中心，配置管理  
Feign 微服务间的远程调用  
gateway网关 外部请求的过滤（服务路由）、身份认证和权限校验、负载均衡、请求限流、CORS跨域配置    

#### 特技

1.  [SpringCloud文档](https://docs.spring.io/spring-cloud-gateway/docs/3.1.0/reference/html/#the-after-route-predicate-factory)  
2.  [SpringCloud+RabbitMQ+Docker+Redis+搜索+分布式，史上最全面的springcloud微服务技术栈课程|黑马程序员Java微服务](https://www.bilibili.com/video/BV1LQ4y127n4?spm_id_from=333.1007.top_right_bar_window_custom_collection.content.click)  
  